load 'nzbmatrix.rb'
load 'tv_interface.rb'
load 'sabnzbd.rb'
require 'yaml'
require 'yaml/store'

class Date
  def self.sday_to_date(s)
    weekdays = [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday]
    day_name = s.to_sym

    if not weekdays.include?(day_name)
      raise ArgumentError, "Not a valid weekday"
    end

    today = Date.today

    offset = (today.wday - weekdays.index(day_name)) % 7
    # ensure that we are always going into the past
    offset = 7 if offset == 0

    return today - offset
  end
end

class Minibeard

  USAGE =
    '
minibeard h             print this help
minibeard t             download episodes for today
minibeard d DAY         download episodes of specific DAY [monday, tuesday, ...]
minibeard dd Date       download episodes of specific date in format dd-mm-yy
minibeard dr DAY        download episodes from last DAY [monday,tuesday ...] to now
minibeard drd DATE      download episodes from DATE in format dd-mm-yy to now
minibeard a             start process to add a new show
minibrard l             list all added episodes
'

  attr_reader :tv

  def initialize(params = {})
    missing = [:nzbmuser, :nzbmapi, :sabapi, :tv_api].select { |param|
      params[param].nil?
    }

    unless missing.empty?
      $stderr.puts "Missing params: #{missing.join(", ")}"
      exit
    end

    @nzb = NZBMatrix.new(params[:nzbmuser], params[:nzbmapi], :http)
    @sab = SABnzbd.new(:api_key => params[:sabapi])
    @tv  = TV_interface.new(params[:tv_api])
    op   = params[:operation]

    puts USAGE if op.empty?

    # h   => help
    # t   => today
    # d   => day
    # dd  => date
    # dr  => day range
    # drd => date range
    # a   => add
    case op.first
    when 'h'
      puts USAGE
    when 't'
      download Date.today
    when 'd'
      download Date.sday_to_date op[1]
    when 'dd'
      download Date.strptime(op[1], '%d-%m-%y')
    when 'dr'
      date.sday_to_date(op[1]).upto Date.today {|date| download date}
    when 'drd'
      date.strptime(op[1], '%d-%m-%y').upto Date.today {|date| download date}
    when 'a'
      @tv.ui_show
    when 'l'
      list
    end
  end

  def list
    @tv.show_a.each do |show|
      puts show.name
    end
  end

  def id_a(date)
    ids = []
    @tv.ep_a(date).each do |show|
      id_sd = @nzb.search(NZBMatrix::TV_SD, "#{show.serie.name} s#{show.seasonno}e#{show.number}").first
      id_hd = @nzb.search(NZBMatrix::TV_HD, "#{show.serie.name} s#{show.seasonno}e#{show.number}").first
      ids.push(id_sd[:error] == "nothing_found" ? id_hd[:nzbid] : id_sd[:nzbid])
    end
    ids
  end

  def download(date)
    id_a(date).each do |id|
      @sab.add_url(@nzb.url(id))
    end
  end
end

ops = []

ARGV.each do |o|
  ops.push(o)
end

ARGV.each do |o|
  ARGV.shift
end

conf = YAML::load_file('./.config')

params = {
  :nzbmuser  => conf["nzbmuser"],
  :nzbmapi   => conf["nzbmapi"],
  :sabapi    => conf["sabapi"],
  :tv_api    => conf["tv_api"],
  :operation => ops #Do not change this line
}

mb = Minibeard.new params
