load 'tv.rb'


class TV_interface

  def initialize(api)
    @api = api
  end

  def ui_show
    tv = Series.new
    puts 'Please enter show name'
    show = gets.delete("\n")
    puts '----------------------'
    puts 'TVDB Search output: '

    index = 0
    shows = tv.find(show)

    shows.each do |s|
      puts "#{index} #{s[:name]} [#{s[:firstaired]}]"
      index = index+1
    end

    puts '---------------------------'
    puts 'Please enter Show ID to add'
    id = gets.delete("\n").to_i
    puts "selected Show is #{shows[id][:name]}"

    Serie.new(shows[id][:id], @api)
  end

  def id_a
    shows = []
    Dir.foreach("./series/") do |file|
      if "0123456789".include? file[0] and file[-3..-1].eql? "xml"
        shows.push(file.split('.')[0])
      end
    end
    shows
  end

  def show_a
    shows = []
    id_a.each do |show|
      shows.push Serie.new(show, @api)
    end
    shows
  end

  def last_season(show)
    show.seasons.last
  end

  def ep_bydate(show, date)
    ep_a = []
    last_season(show).reverse_each do |x|
      if not x.first_aired.nil?
        if Date.strptime(x.first_aired, '%Y-%m-%d') === date.prev_day
          ep_a.push(x)
        end
      end
    end
    ep_a
  end

  def ep_a(date)
    ep_a = []
    show_a.each do |show|
      ep = ep_bydate(show, date)
      if not ep.empty?
        ep_a.concat ep
      end
    end
    ep_a
  end
end




